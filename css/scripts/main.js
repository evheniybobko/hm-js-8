const paragraphs = document.querySelectorAll("p");
paragraphs.forEach(item => item.style.backgroundColor = "#ff0000");

const optList = document.getElementById("optionsList");
console.log(optList);

const optListParent = optList.parentElement;
console.log(optListParent);


if (optList.hasChildNodes()) {
    const optListChildren = optList.childNodes;
    optListChildren.forEach(item => {
        console.log(item.nodeName, item.nodeType);
    })
};
    
let newText = document.getElementById("testParagraph");
newText.textContent = "This is a paragraph";

for (let item of document.querySelector(".main-header").children) {
    console.log(item);
    item.classList.add("nav-item");
};

for (let item of document.querySelectorAll(".section-title")) {
    item.classList.remove("section-title");
}




/* 1. Опишіть своїми словами що таке Document Object Model (DOM)

Це програмний інтерфейс, який забезпечує вивід документу як структурованої групи вузлів і об'єктів (дерева).
Вони мають свої властивості і методи, які забепечують доступність даних для програми, можливість зміни документу, його стилів і вмісту.
 */


/* 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

- innerHTML отримує та встановлює вміст у форматі HTML і відображає теги html всередині цього вмісту, якщо вони є.
- innerText отримує та встановлює вміст тегу як простий текст, у вигляді рядка, застосовує до нього css-стилі 
і приховує теги html, якщо вони є у вмісті тегу.
 */


/* 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

Є 6 основних способів звернутися, знайти елемент на сторінці:
- querySelector(css);
- querySelectorAll(css);
- getElementById(id);
- getElementbyName(name);
- getElementbyTagName(tag or "*");
- getElementbyClassName(class).
Найпотужніший і найчастіше використовується, бо майже універсальний - querySelectorAll(css). 
Може використовувати будь-який css-selector, навіть псевдокласи.
 */